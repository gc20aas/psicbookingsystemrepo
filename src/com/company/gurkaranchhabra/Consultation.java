package com.company.gurkaranchhabra;

public class Consultation{

    private Room room;
    private Physician physician;
    private String dateTimePeriod;
    private String visitorName;

    public Consultation(String roomName,Physician newPhysician, String dateTimePeriod){
        this.room = new Room(roomName);
        this.physician = newPhysician;
        this.setDateTimePeriod(dateTimePeriod);
    }

    public void setVisitorName(String name){
        this.visitorName = name;
    }

    public String getVisitorName(){
        return this.visitorName;
    }

    public void setConsultationRoomName(String roomName){
        this.room.setRoomName(roomName);
    }

    public String getConsultationRoomName(){
        return this.room.getRoomName();
    }

    public void setConsultationPhysician(Physician newPhysician){
        this.physician = newPhysician;
    }

    public Physician getConsultationPhysician(){
        return this.physician;
    }

    public void setDateTimePeriod(String dateTimePeriod){
        this.dateTimePeriod = dateTimePeriod;
    }

    public String getDateTimePeriod(){
        return this.dateTimePeriod;
    }

    public String toString(){
        return this.physician.getFullName() + ", at " + this.room.getRoomName() + ", on " + this.dateTimePeriod;
    }
}
