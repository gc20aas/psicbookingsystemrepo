package com.company.gurkaranchhabra;

import java.util.ArrayList;

public class Physician extends Patient{
    private String areaOfExpertise;
    private String consultationHours;
    private static int id = 1;

    public Physician( String number, String name, String address, String areaOfExpertise, String consultationHours){
        super(number,name,address);
        setAreaOfExpertise(areaOfExpertise);
        this.setConsultationHours(consultationHours);
    }


    public void setId(){
        this.id = id;
        id++;
    }

    public int getId(){
        return this.id;
    }

    public void setAreaOfExpertise(String areaOfExpertiseInput){
        this.areaOfExpertise = areaOfExpertiseInput;
    }

    public String getAreaOfExpertise(){
        return this.areaOfExpertise;
    }

    public void setConsultationHours(String consultationHours){
        this.consultationHours = consultationHours;
    }

    public String getConsultationHours(){
        return this.consultationHours;
    }



}
