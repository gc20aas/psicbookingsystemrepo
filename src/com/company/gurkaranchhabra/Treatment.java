package com.company.gurkaranchhabra;

public class Treatment {
    private String name;
    private Room room;
    private Physician physician;
    private String dateTimePeriod;
    private String bookingStatus;
    private Patient patient;

    public Treatment(String name,String roomName,Physician newPhysician, String dateTimePeriod,String bookingStatus){
        this.setTreatmentName(name);
        this.room = new Room(roomName);
        this.physician = newPhysician;
        this.setDateTimePeriod(dateTimePeriod);
        this.setBookingStatus(bookingStatus);
    }

    public void setPatient(Patient newPatient){
        this.patient = newPatient;
    }

    public Patient getPatient(){
        return this.patient;
    }

    public void setTreatmentName(String name){
        this.name = name;
    }

    public String getTreatmentName(){
        return this.name;
    }

    public void setTreatmentRoomName(String roomName){
        this.room.setRoomName(roomName);
    }

    public void setBookingStatus(String status){
        this.bookingStatus = status;
    }

    public String getBookingStatus(){
        return this.bookingStatus;
    }

    public String getTreatmentRoomName(){
        return this.room.getRoomName();
    }

    public void setTreatmentPhysician(Physician newPhysician){
        this.physician = newPhysician;
    }

    public Physician getTreatmentPhysician(){
        return this.physician;
    }

    public void setDateTimePeriod(String dateTimePeriod){
        this.dateTimePeriod = dateTimePeriod;
    }

    public String getDateTimePeriod(){
        return this.dateTimePeriod;
    }

    public String toString(){
        return "The treatment " + this.getTreatmentName() + " by Physician " + this.physician.getFullName() + " in room " + this.room.getRoomName() + " on " + this.dateTimePeriod + " is " + this.getBookingStatus() + ".";
    }
}
