package com.company.gurkaranchhabra;





import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class BookingTest{
    private Patient jesse;
    private Physician atwell;
    private Room r;
    private Consultation jesseConsultation;
    private Treatment atwellTreatment;

    @BeforeEach
    public void setUp(){
        jesse = new Patient("8127145059","Jesse Smith","4664 Heliport Loop Bloomington, IN 47408");
        atwell = new Physician("","Stephen Atwell","92 Ponteland Rd HOWSHAM LN7 5PU","Homeopathy","Friday 12-2 pm");
        r = new Room("Consulting Suite A");
        jesseConsultation = new Consultation("Consulting Suite B",atwell,"Wednesday 5th May 2021, 14:00-14:30");
        atwellTreatment = new Treatment("Neural mobilisation","Consulting Suite A",atwell,"Saturday 1st May 2021, 11:00-13:00","Available");
    }


    @Test
    public void getPatientTelephoneNumber() {
        assertEquals("8127145059",jesse.getTelephoneNumber());
    }

    @Test
    public void getRoomName(){
        assertEquals("Consulting Suite A",r.getRoomName());
    }

    @Test
    public void changeRoomName(){
        r.setRoomName("Gym");
        assertEquals("Gym",r.getRoomName());
    }

    @Test
    public void setTreatmentPatient(){
        atwellTreatment.setPatient(jesse);
        assertEquals("Jesse Smith",atwellTreatment.getPatient().getFullName());
    }

    @Test
    public void setVisitorName(){
        jesseConsultation.setVisitorName("Mark Cuban");
        assertEquals("Mark Cuban",jesseConsultation.getVisitorName());
    }

    @Test
    public void getTreatmentPhysicianFullName(){
        assertEquals("Stephen Atwell",atwellTreatment.getTreatmentPhysician().getFullName());
    }

    @Test
    public void getPhysicianConsultationHours(){
        assertEquals("Friday 12-2 pm",atwell.getConsultationHours());
    }

    @Test
    public void getConsultationPhysicianExpertise(){
        assertEquals("Homeopathy",jesseConsultation.getConsultationPhysician().getAreaOfExpertise());
    }

    @Test
    public void checkPhysicianDetails(){
        assertEquals(jesseConsultation.getConsultationPhysician().getFullName(),atwellTreatment.getTreatmentPhysician().getFullName());
    }

    @Test
    public void setTreatmentBookingStatus(){
        atwellTreatment.setBookingStatus("Booked");
        assertEquals("Booked",atwellTreatment.getBookingStatus());
    }

}