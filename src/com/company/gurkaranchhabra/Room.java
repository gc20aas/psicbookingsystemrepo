package com.company.gurkaranchhabra;

public class Room {
    private String roomName;
    public Room(String roomName){
        this.roomName = roomName;
    }

    public void setRoomName(String roomName){
        this.roomName =  roomName;
    }

    public String getRoomName(){
        return this.roomName;
    }
}
