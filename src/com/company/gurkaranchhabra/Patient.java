package com.company.gurkaranchhabra;

public class Patient {
    private static int id = 1;
    private String fullName;
    private String address;
    private String telephoneNumber;

    public Patient(String number,String name, String address){
        this.setId();
        this.setFullName(name);
        this.setAddress(address);
        this.setTelephoneNumber(number);
    }

    public void setId(){
        this.id = id;
        id++;
    }

    public int getId(){
        return this.id;
    }

    public void setFullName(String name){
        this.fullName = name;
    }

    public String getFullName(){
        return this.fullName;
    }

    public void setAddress(String address){
        this.address = address;
    }

    public String getAddress(){
        return this.address;
    }

    public void setTelephoneNumber(String telephoneNumber){
        this.telephoneNumber = telephoneNumber;
    }

    public String getTelephoneNumber(){
        return this.telephoneNumber;
    }
}
