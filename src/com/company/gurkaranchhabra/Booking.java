package com.company.gurkaranchhabra;

import java.util.*;

public class Booking {
    static Scanner scanner = new Scanner(System.in);
    static final int NUMBER_OF_PHYSICIANS = 6;
    static final int NUMBER_OF_TREATMENTS = 70;
    static int NUMBER_OF_PATIENTS = 15;
    static final int NUMBER_OF_CONSULTATION_APPOINTMENTS = 37;
    static Physician[] physicianList = new Physician[NUMBER_OF_PHYSICIANS];
    static Patient[] patientList = new Patient[NUMBER_OF_PATIENTS];
    static ArrayList<Patient> patientArrayList = new ArrayList<Patient>();
    static Treatment[] availableTreatments = new Treatment[NUMBER_OF_TREATMENTS];
    static ArrayList<Treatment> availableTreatmentsList = new ArrayList<Treatment>();
    static ArrayList <Treatment> bookedTreatmentsList = new ArrayList<Treatment>();
    static Consultation[] availableConsultationAppointments = new Consultation[NUMBER_OF_CONSULTATION_APPOINTMENTS];
    static ArrayList<Consultation> availableConsultationAppointmentsList = new ArrayList<Consultation>();
    static ArrayList<Consultation> bookedConsultationAppointments = new ArrayList <Consultation>();
    static ArrayList<String> visitorNames = new ArrayList<String>();


    public static void visitorBookingSystem(String patientName){
        boolean visitorBookingLoop = true;
        do{
            System.out.println("Enter a Physician's expertise or a Physician's name: ");
            String selection = scanner.nextLine();
            ArrayList<Consultation> physiciansList = findPhysicianConsultation(selection);
            if(physiciansList == null){
                System.out.println("Sorry, Physician not found or no available appointments with this Physician. Try Again!");
                continue;
            }else {

                for(int i = 0; i < physiciansList.size(); i++){
                    System.out.println(i + 1 + ". " + physiciansList.get(i).toString());
                }

                System.out.println("Enter the appropriate number to book the appointment.");
                int appointmentSelection = Integer.valueOf(scanner.nextLine());

                for (int i = 0; i < physiciansList.size(); i++) {
                    if(appointmentSelection - 1 == i) {
                        System.out.println(physiciansList.get(i));
                        bookedConsultationAppointments.add(physiciansList.get(i));
                        availableConsultationAppointmentsList.remove(physiciansList.get(i));
                        visitorNames.add(patientName);
                        physiciansList.get(i).setVisitorName(patientName);
                        System.out.println("Appointment Booked!");
                        visitorNames.add(patientName);
                        visitorBookingLoop = false;
                    }
                }
            }
        }
        while(visitorBookingLoop);
    }

    public static ArrayList<Consultation> findPhysicianConsultation(String expertiseOrName){
        ArrayList<Consultation> availablePhysicians = new ArrayList<Consultation>();
        if(availableConsultationAppointmentsList.size() > 0){
            for(int i = 0; i < availableConsultationAppointmentsList.size();i++){
                if(availableConsultationAppointmentsList.get(i).getConsultationPhysician().getFullName().equals(expertiseOrName) || availableConsultationAppointmentsList.get(i).getConsultationPhysician().getAreaOfExpertise().equals(expertiseOrName)){
                    availablePhysicians.add(availableConsultationAppointmentsList.get(i));
                }
            }
        }

        if(availablePhysicians != null){
            return availablePhysicians;
        }
        return null;
    }

    public static void patientBookingSystem(){
        System.out.println("Are you a Registered Patient?");
        System.out.println("1. Yes");
        System.out.println("2. Register as a Patient");
        System.out.println("3. Quit");

        int patientChoice = Integer.valueOf(scanner.nextLine());
        if(patientChoice == 1){
            Boolean patientLoop = true;
            do{
                System.out.println("Please enter your name: ");
                String patientName = scanner.nextLine();

                if(lookUpPatientByName(patientName) != null){
                    Patient currentPatient = lookUpPatientByName(patientName);
                    ArrayList<Treatment> patientAppointments = lookUpTreatmentByPatient(currentPatient);
                    if(patientAppointments == null){
                        System.out.println("No Bookings found for Patient " + patientName);
                        System.out.println("Enter a Physician's expertise or a Physician's name: ");
                        String expertiseOrName = scanner.nextLine();
                        if(findPhysicianTreatment(expertiseOrName) != null){
                            ArrayList<Treatment> physiciansList = findPhysicianTreatment(expertiseOrName);
                            for(int i = 0; i < physiciansList.size();i++){
                                System.out.println(i + 1 + ". " + physiciansList.get(i).toString());
                            }

                            System.out.println("Enter the appropriate number to book the appointment.");
                            int appointmentSelection = Integer.valueOf(scanner.nextLine());

                            for(int i = 0; i < physiciansList.size();i++){
                                if(appointmentSelection - 1 == i){
                                    System.out.println(physiciansList.get(i));
                                    physiciansList.get(i).setPatient(currentPatient);
                                    physiciansList.get(i).setBookingStatus("Booked");
                                    bookedTreatmentsList.add(physiciansList.get(i));
                                    availableTreatmentsList.remove(physiciansList.get(i));
                                    System.out.println("Appointment Booked!");
                                    patientLoop = false;
                                }
                            }
                        }else{
                            System.out.println("Sorry, Physician not found or no available appointments with this Physician. Try Again!");
                            continue;
                        }
                    }else{
                        System.out.println("1. Book a new Appointment");
                        for(int i = 0; i < patientAppointments.size();i++){
                            System.out.println(i + 2 + ". " + patientAppointments.get(i).toString());
                        }
                        System.out.println("Enter the appropriate number: ");
                        int selection = Integer.valueOf(scanner.nextLine());
                        if(selection == 1){
                            System.out.println("Enter a Physician's expertise or a Physician's name: ");
                            String expertiseOrName = scanner.nextLine();
                            if(findPhysicianTreatment(expertiseOrName) != null){
                                ArrayList<Treatment> physiciansList = findPhysicianTreatment(expertiseOrName);
                                for(int j = 0; j < physiciansList.size();j++){
                                    System.out.println(j + 1 + ". " + physiciansList.get(j).toString());
                                }

                                System.out.println("Enter the appropriate number to book the appointment.");
                                int appointmentSelection = Integer.valueOf(scanner.nextLine());

                                for(int k = 0; k < physiciansList.size();k++) {
                                    if (appointmentSelection - 1 == k) {
                                        System.out.println(physiciansList.get(k));
                                        physiciansList.get(k).setPatient(currentPatient);
                                        physiciansList.get(k).setBookingStatus("Booked");
                                        bookedTreatmentsList.add(physiciansList.get(k));
                                        availableTreatmentsList.remove(physiciansList.get(k));
                                        System.out.println("Appointment Booked!");
                                        patientLoop = false;
                                    }
                                }
                            }
                        }
                        for(int i = 0; i < patientAppointments.size();i++){
                            if(selection - 2 == i && selection != 1){
                                System.out.println("1. Cancel Appointment");
                                System.out.println("2. Attend Appointment");
                                System.out.println("3. Change Appointment");
                                int choice = Integer.valueOf(scanner.nextLine());
                                if(choice == 1){
                                    bookedTreatmentsList.remove(patientAppointments.get(i));
                                    availableTreatmentsList.add(patientAppointments.get(i));
                                    patientAppointments.get(i).setPatient(null);
                                    patientAppointments.get(i).setBookingStatus("Cancelled");
                                    System.out.println("Appointment Cancelled!");
                                }else if(choice == 2){
                                    patientAppointments.get(i).setBookingStatus("Attended");
                                    System.out.println("Appointment attended!");
                                }else if(choice == 3){
                                    patientLoop = false;
                                }
                            }
                        }
                    }
                }
                else{
                    System.out.println("Can't find a patient with name " + patientName + " in our directory.");
                    System.out.println("1. Retry");
                    System.out.println("2. Quit");
                    int input = Integer.valueOf(scanner.nextLine());
                    if(input == 1){
                        continue;
                    }else if(input == 2){
                        patientLoop = false;
                    }

                }
            }while(patientLoop);
        }else if(patientChoice == 2){
            System.out.println("Please Enter your Full Name: ");
            String fullName = scanner.nextLine();
            String arr[] = fullName.split(" ",2);

            System.out.println("Please Enter your Phone Number: ");
            String number = scanner.nextLine();
            System.out.println("Please Enter your address: ");
            String address = scanner.nextLine();

            patientArrayList.add(new Patient(number,fullName,address));


        }else if(patientChoice == 3){

        }
    }

    public static ArrayList<Treatment> findPhysicianTreatment(String expertiseOrName){
        ArrayList<Treatment> availablePhysicians = new ArrayList<Treatment>();
        if(availableTreatmentsList.size() > 0){
            for(int i = 0; i < availableTreatmentsList.size();i++){
                if(availableTreatmentsList.get(i).getTreatmentPhysician().getFullName().equals(expertiseOrName) || availableTreatmentsList.get(i).getTreatmentPhysician().getAreaOfExpertise().equals(expertiseOrName)){
                    availablePhysicians.add(availableTreatmentsList.get(i));
                }
            }
        }

        if(availablePhysicians != null){
            return availablePhysicians;
        }
        return null;
    }

    public static ArrayList<Treatment> lookUpTreatmentByPatient(Patient patient){
        if(bookedTreatmentsList.size() == 0){
            return null;
        }
        ArrayList<Treatment> treatmentList = new ArrayList<Treatment>();
        for(Treatment t: bookedTreatmentsList){
            if(t.getPatient() == patient){
                treatmentList.add(t);
            }
        }
        if(treatmentList != null){
            return treatmentList;
        }
        return null;
    }

    public static Patient lookUpPatientByName(String patientName){
        for(Patient p: patientArrayList){
            if(p.getFullName().equals(patientName)){
                return p;
            }
        }
        return null;
    }

    public static void report1(){
        System.out.println("Treatment Appointments: ");
        for(Treatment t: availableTreatmentsList){
            System.out.println(t);
        }
        System.out.println("");
        System.out.println("Visitor Appointments: ");
        for(Consultation c: availableConsultationAppointmentsList){
            System.out.println(c);
        }
    }

    public static void report2(){
        for(Patient p: patientArrayList){
            System.out.println(p.getFullName() + ": ");
            if(bookedTreatmentsList.size() > 0){
                for(Treatment t: bookedTreatmentsList){
                    if(t.getPatient() == p){
                        System.out.println("           " + t);
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        Physician robert = new Physician("07770596198","Erin McLean","69 Thornton St HUNGARTON LE7 9ZY","Physiotherapy","Saturday 10-12 am");
        Physician andhrew = new Physician("07036109378","Andrew Harris","82 Rowland Rd OTHERY TA7 9ND","Osteopathy","Monday 12-14 pm");
        Physician joe = new Physician("07045105814","Joe Akhtar","43 Whitchurch Road ELTISLEY PE19 3BW","Rehabilitation", "Tuesday 9-11 am");
        Physician elise = new Physician("07903026608","Elise Hurst","14 Duckpit Lane UPPINGHAM LE15 9WJ","Osteopathy","Wednesday 14-16 pm");
        Physician jessica = new Physician("07705190471","Jessica Cook","44 Prince Consort Road KENT STREET ME18 8SU","Homeopathy","Thursday 13-15 pm");
        Physician jay = new Physician("07724797711","Jay Williamson","2 Red Lane ERIFF DG7 2HY","Rehabilitation","Friday 10-12 am");

        physicianList[0] = robert;
        physicianList[1] = andhrew;
        physicianList[2] = joe;
        physicianList[3] = elise;
        physicianList[4] = jessica;
        physicianList[5] = jay;

        Consultation robertConsultation1 = new Consultation("Consulting Suite B",robert,"Saturday 1st May 2021, 10:00-10:30");
        Consultation robertConsultation2 = new Consultation("Consulting Suite A",robert,"Saturday 8th May 2021, 10:00-10:30");
        Consultation robertConsultation3 = new Consultation("Consulting Suite A",robert,"Saturday 8th May 2021, 10:30-11:00");
        Consultation robertConsultation4 = new Consultation("Consulting Suite A",robert,"Saturday 22th May 2021, 10:00-10:30");
        Consultation robertConsultation5 = new Consultation("Consulting Suite A",robert,"Saturday 22th May 2021, 10:30-11:00");
        Consultation robertConsultation6 = new Consultation("Consulting Suite A",robert,"Saturday 22th May 2021, 11:30-12:00");

        Consultation andhrewConsultation1 = new Consultation("Swimming Pool",andhrew,"Monday 3rd May 2021, 12:00-12:30");
        Consultation andhrewConsultation2 = new Consultation("Swimming Pool",andhrew,"Monday 10th May 2021, 12:00-12:30");
        Consultation andhrewConsultation3 = new Consultation("Swimming Pool",andhrew,"Monday 10th May 2021, 12:30-13:00");
        Consultation andhrewConsultation4 = new Consultation("Consulting Suite C",andhrew,"Monday 10th May 2021, 13:00-13:30");
        Consultation andhrewConsultation5 = new Consultation("Swimming Pool",andhrew,"Monday 17th May 2021, 12:30-13:00");
        Consultation andhrewConsultation6 = new Consultation("Consulting Suite C",andhrew,"Monday 24th May 2021, 12:00-12:30");
        Consultation andhrewConsultation7 = new Consultation("Consulting Suite C",andhrew,"Monday 24th May 2021, 12:30-13:00");

        Consultation joeConsultation1 = new Consultation("Gym",joe,"Tuesday 4th May 2021, 9:00-9:30");
        Consultation joeConsultation2 = new Consultation("Gym",joe,"Tuesday 4th May 2021, 9:30-10:00");
        Consultation joeConsultation3 = new Consultation("Gym",joe,"Tuesday 11th May 2021, 9:00-9:30");
        Consultation joeConsultation4 = new Consultation("Gym",joe,"Tuesday 18th May 2021, 9:00-9:30");
        Consultation joeConsultation5 = new Consultation("Gym",joe,"Tuesday 25th May 2021, 10:00-10:30");

        Consultation eliseConsultation1 = new Consultation("Consulting Suite B",elise,"Wednesday 5th May 2021, 14:00-14:30");
        Consultation eliseConsultation2 = new Consultation("Consulting Suite B",elise,"Wednesday 5th May 2021, 14:30-15:00");
        Consultation eliseConsultation3 = new Consultation("Consulting Suite A",elise,"Wednesday 12th May 2021, 14:00-14:30");
        Consultation eliseConsultation4 = new Consultation("Consulting Suite A",elise,"Wednesday 12th May 2021, 14:30-15:00");
        Consultation eliseConsultation5 = new Consultation("Consulting Suite B",elise,"Wednesday 19th May 2021, 14:00-14:30");
        Consultation eliseConsultation6 = new Consultation("Consulting Suite B",elise,"Wednesday 19th May 2021, 14:30-15:00");
        Consultation eliseConsultation7 = new Consultation("Consulting Suite A",elise,"Wednesday 26th May 2021, 14:00-14:30");
        Consultation eliseConsultation8 = new Consultation("Consulting Suite A",elise,"Wednesday 26th May 2021, 14:30-15:00");

        Consultation jessicaConsultation1 = new Consultation("Consulting Suite B",jessica,"Thursday 6th May 2021, 13:00-13:30");
        Consultation jessicaConsultation2 = new Consultation("Consulting Suite B",jessica,"Thursday 6th May 2021, 13:30-14:00");
        Consultation jessicaConsultation3 = new Consultation("Consulting Suite B",jessica,"Thursday 13th May 2021, 13:00-13:30");
        Consultation jessicaConsultation4 = new Consultation("Consulting Suite B",jessica,"Thursday 20th May 2021, 14:00-14:30");
        Consultation jessicaConsultation5 = new Consultation("Consulting Suite B",jessica,"Thursday 20th May 2021, 14:30-15:00");

        Consultation jayConsultation1 = new Consultation("Gym",jay,"Friday 7th May 2021, 10:00-10:30");
        Consultation jayConsultation2 = new Consultation("Gym",jay,"Friday 7th May 2021, 10:30-11:00");
        Consultation jayConsultation3 = new Consultation("Consulting Suite C",jay,"Friday 14th May 2021, 10:00-10:30");
        Consultation jayConsultation4 = new Consultation("Gym",jay,"Friday 21st May 2021, 10:30-11:00");
        Consultation jayConsultation5 = new Consultation("Consulting Suite C",jay,"Friday 21st May 2021, 11:30-12:00");
        Consultation jayConsultation6 = new Consultation("Gym",jay,"Friday 28th May 2021, 10:00-10:30");

        availableConsultationAppointments[0] = robertConsultation1;
        availableConsultationAppointments[1] = robertConsultation2;
        availableConsultationAppointments[2] = robertConsultation3;
        availableConsultationAppointments[3] = robertConsultation4;
        availableConsultationAppointments[4] = robertConsultation5;
        availableConsultationAppointments[5] = robertConsultation6;
        availableConsultationAppointments[6] = andhrewConsultation1;
        availableConsultationAppointments[7] = andhrewConsultation2;
        availableConsultationAppointments[8] = andhrewConsultation3;
        availableConsultationAppointments[9] = andhrewConsultation4;
        availableConsultationAppointments[10] = andhrewConsultation5;
        availableConsultationAppointments[11] = andhrewConsultation6;
        availableConsultationAppointments[12] = andhrewConsultation7;
        availableConsultationAppointments[13] = joeConsultation1;
        availableConsultationAppointments[14] = joeConsultation2;
        availableConsultationAppointments[15] = joeConsultation3;
        availableConsultationAppointments[16] = joeConsultation4;
        availableConsultationAppointments[17] = joeConsultation5;
        availableConsultationAppointments[18] = eliseConsultation1;
        availableConsultationAppointments[19] = eliseConsultation2;
        availableConsultationAppointments[20] = eliseConsultation3;
        availableConsultationAppointments[21] = eliseConsultation4;
        availableConsultationAppointments[22] = eliseConsultation5;
        availableConsultationAppointments[23] = eliseConsultation6;
        availableConsultationAppointments[24] = eliseConsultation7;
        availableConsultationAppointments[25] = eliseConsultation8;
        availableConsultationAppointments[26] = jessicaConsultation1;
        availableConsultationAppointments[27] = jessicaConsultation2;
        availableConsultationAppointments[28] = jessicaConsultation3;
        availableConsultationAppointments[29] = jessicaConsultation4;
        availableConsultationAppointments[30] = jessicaConsultation5;
        availableConsultationAppointments[31] = jayConsultation1;
        availableConsultationAppointments[32] = jayConsultation2;
        availableConsultationAppointments[33] = jayConsultation3;
        availableConsultationAppointments[34] = jayConsultation4;
        availableConsultationAppointments[35] = jayConsultation5;
        availableConsultationAppointments[36] = jayConsultation6;

        availableConsultationAppointmentsList = new ArrayList<Consultation>(Arrays.asList(availableConsultationAppointments));

        Patient elliot = new Patient("07004377321","Elliot Morton","59 Long Street MILNSBRIDGE HD4 5UU");
        Patient oliver = new Patient("07089539515","Oliver Simpson","24 Helland Bridge UPPER BIGHOUSE KW13 9XL");
        Patient jordan = new Patient("07854781294","Jordan Cross","20 Park Avenue LEA TOWN PR4 1XA");
        Patient kate = new Patient("07812988950","Kate Carey","80 Gloucester Road CLANDOWN BA3 2HS");
        Patient daisy = new Patient("07941434059","Daisy Weston","40 Uxbridge Road SLIOCH AB54 8TA");
        Patient joshua = new Patient("07733822769","Joshua Hunter","18 Well Lane PEASENHALL IP17 0BY");
        Patient ben = new Patient("07078796253","Ben Howard","93 Abbey Row OAKENGATES TF2 2AQ");
        Patient jonathan = new Patient("07071941704","Jonathan Crawford","63 Earls Avenue WHITEWAY GL6 4XJ");
        Patient lewis = new Patient("07877925631","Lewis Mason","11 Iffley Road BROADBRIDGE PO18 8YX");
        Patient sarah = new Patient("07723999491","Sarah Farrell","81 London Road COMPTON ABBAS SP7 7PJ");
        Patient jack = new Patient("07040270283","Jack Wyatt","27 Emerson Road KIRK IRETON DE6 7QA");
        Patient leo = new Patient("07766041046","Leo Archer","84 Eastbourne Rd COLLINGBOURNE KINGSTON SN8 9WA");
        Patient amy = new Patient("07982295431","Amy Norman","54 Trinity Crescent WHEATLEY LANE BB12 4NG");
        Patient lauren = new Patient("07924080939","Lauren Fleming","31 Wade Lane SALHOUSE NR13 7UF");
        Patient thomas = new Patient("07035052742","Thomas Roberts","52 Chapel Lane ARRINGTON SG8 8YW");

        patientList[0] = elliot;
        patientList[1] = oliver;
        patientList[2] = jordan;
        patientList[3] = kate;
        patientList[4] = daisy;
        patientList[5] = joshua;
        patientList[6] = ben;
        patientList[7] = jonathan;
        patientList[8] = lewis;
        patientList[9] = sarah;
        patientList[10] = jack;
        patientList[11] = leo;
        patientList[12] = amy;
        patientList[13] = lauren;
        patientList[14] = thomas;

        patientArrayList = new ArrayList<Patient>(Arrays.asList(patientList));

        Treatment robertTreatment1 = new Treatment("Neural mobilisation","Consulting Suite A",robert,"Saturday 1st May 2021, 11:00-13:00","Available");
        Treatment robertTreatment2 = new Treatment("Neural mobilisation","Consulting Suite A",robert,"Saturday 1st May 2021, 14:00-16:00","Available");
        Treatment robertTreatment3 = new Treatment("Acupuncture","Consulting Suite B",robert,"Monday 3rd May 2021, 12:00-14:00","Available");
        Treatment robertTreatment4 = new Treatment("Neural mobilisation","Consulting Suite A",robert,"Saturday 8th May 2021, 11:00-13:00","Available");
        Treatment robertTreatment5 = new Treatment("Neural mobilisation","Consulting Suite A",robert,"Saturday 8th May 2021, 14:00-16:00","Available");
        Treatment robertTreatment6 = new Treatment("Acupuncture","Consulting Suite B",robert,"Monday 10th May 2021, 12:00-14:00","Available");
        Treatment robertTreatment7 = new Treatment("Neural mobilisation","Consulting Suite A",robert,"Saturday 15th May 2021, 11:00-13:00","Available");
        Treatment robertTreatment8 = new Treatment("Neural mobilisation","Consulting Suite A",robert,"Saturday 15th May 2021, 14:00-16:00","Available");
        Treatment robertTreatment9 = new Treatment("Acupuncture","Consulting Suite B",robert,"Monday 17th May 2021, 12:00-14:00","Available");
        Treatment robertTreatment10 = new Treatment("Neural mobilisation","Consulting Suite A",robert,"Saturday 22nd May 2021, 11:00-13:00","Available");
        Treatment robertTreatment11 = new Treatment("Neural mobilisation","Consulting Suite A",robert,"Saturday 22nd May 2021, 14:00-16:00","Available");
        Treatment robertTreatment12 = new Treatment("Acupuncture","Consulting Suite B",robert,"Monday 24th May 2021, 12:00-14:00","Available");
        Treatment robertTreatment13 = new Treatment("Acupuncture","Consulting Suite B",robert,"Monday 31st May 2021, 12:00-14:00","Available");

        Treatment andhrewTreatment1 = new Treatment("Mobilisation of the spine and joints","Consulting Suite C",andhrew,"Monday 3rd May 2021, 10:00-12:00","Available");
        Treatment andhrewTreatment2 = new Treatment("Mobilisation of the spine and joints","Consulting Suite C",andhrew,"Monday 3rd May 2021, 13:00-15:00","Available");
        Treatment andhrewTreatment3 = new Treatment("Massage","Consulting Suite B",andhrew,"Tuesday 4th May 2021, 12:00-14:00","Available");
        Treatment andhrewTreatment4 = new Treatment("Mobilisation of the spine and joints","Consulting Suite C",andhrew,"Monday 10th May 2021, 10:00-12:00","Available");
        Treatment andhrewTreatment5 = new Treatment("Mobilisation of the spine and joints","Consulting Suite C",andhrew,"Monday 10th May 2021, 13:00-15:00","Available");
        Treatment andhrewTreatment6 = new Treatment("Massage","Consulting Suite B",andhrew,"Tuesday 11st May 2021, 12:00-14:00","Available");
        Treatment andhrewTreatment7 = new Treatment("Mobilisation of the spine and joints","Consulting Suite C",andhrew,"Monday 17th May 2021, 10:00-12:00","Available");
        Treatment andhrewTreatment8 = new Treatment("Mobilisation of the spine and joints","Consulting Suite C",andhrew,"Monday 17th May 2021, 13:00-15:00","Available");
        Treatment andhrewTreatment9 = new Treatment("Massage","Consulting Suite B",andhrew,"Tuesday 18th May 2021, 12:00-14:00","Available");
        Treatment andhrewTreatment10 = new Treatment("Mobilisation of the spine and joints","Consulting Suite C",andhrew,"Monday 24th May 2021, 10:00-12:00","Available");
        Treatment andhrewTreatment11 = new Treatment("Mobilisation of the spine and joints","Consulting Suite C",andhrew,"Monday 24th May 2021, 13:00-15:00","Available");
        Treatment andhrewTreatment12 = new Treatment("Massage","Consulting Suite B",andhrew,"Tuesday 25th May 2021, 12:00-14:00","Available");

        Treatment joeTreatment1 = new Treatment("Pool rehabilitation","Swimming Pool",joe,"Wednesday 5th May 2021, 15:00-17:00","Available");
        Treatment joeTreatment2 = new Treatment("Pool rehabilitation","Swimming Pool",joe,"Thursday 6th May 2021, 15:00-17:00","Available");
        Treatment joeTreatment3 = new Treatment("Massage","Gym",joe,"Friday 7th May 2021, 15:00-17:00","Available");
        Treatment joeTreatment4 = new Treatment("Pool rehabilitation","Swimming Pool",joe,"Wednesday 12th May 2021, 15:00-17:00","Available");
        Treatment joeTreatment5 = new Treatment("Pool rehabilitation","Swimming Pool",joe,"Thursday 13th May 2021, 15:00-17:00","Available");
        Treatment joeTreatment6 = new Treatment("Massage","Gym",joe,"Friday 14th May 2021, 15:00-17:00","Available");
        Treatment joeTreatment7 = new Treatment("Pool rehabilitation","Swimming Pool",joe,"Wednesday 19th May 2021, 15:00-17:00","Available");
        Treatment joeTreatment8 = new Treatment("Pool rehabilitation","Swimming Pool",joe,"Thursday 20th May 2021, 15:00-17:00","Available");
        Treatment joeTreatment9 = new Treatment("Massage","Gym",joe,"Friday 21st May 2021, 15:00-17:00","Available");
        Treatment joeTreatment10 = new Treatment("Pool rehabilitation","Swimming Pool",joe,"Wednesday 26th May 2021, 15:00-17:00","Available");
        Treatment joeTreatment11 = new Treatment("Pool rehabilitation","Swimming Pool",joe,"Thursday 27th May 2021, 15:00-17:00","Available");
        Treatment joeTreatment12 = new Treatment("Massage","Gym",joe,"Friday 28th May 2021, 15:00-17:00","Available");

        Treatment eliseTreatment1 = new Treatment("Mobilisation of the spine and joints","Consulting Suite C",elise,"Monday 3rd May 2021, 16:00-18:00","Available");
        Treatment eliseTreatment2 = new Treatment("Acupuncture","Consulting Suite B",elise,"Tuesday 4th May 2021, 13:00-15:00","Available");
        Treatment eliseTreatment3 = new Treatment("Neural mobilisation","Consulting Suite B",elise,"Wednesday 5th May 2021, 10:00-12:00","Available");
        Treatment eliseTreatment4 = new Treatment("Mobilisation of the spine and joints","Consulting Suite C",elise,"Monday 10th May 2021, 16:00-18:00","Available");
        Treatment eliseTreatment5 = new Treatment("Acupuncture","Consulting Suite B",elise,"Tuesday 11st May 2021, 13:00-15:00","Available");
        Treatment eliseTreatment6 = new Treatment("Neural mobilisation","Consulting Suite B",elise,"Wednesday 12th May 2021, 10:00-12:00","Available");
        Treatment eliseTreatment7 = new Treatment("Mobilisation of the spine and joints","Consulting Suite C",elise,"Monday 17th May 2021, 16:00-18:00","Available");
        Treatment eliseTreatment8 = new Treatment("Acupuncture","Consulting Suite B",elise,"Tuesday 18th May 2021, 13:00-15:00","Available");
        Treatment eliseTreatment9 = new Treatment("Neural mobilisation","Consulting Suite B",elise,"Wednesday 19th May 2021, 10:00-12:00","Available");
        Treatment eliseTreatment10 = new Treatment("Mobilisation of the spine and joints","Consulting Suite C",elise,"Monday 24th May 2021, 16:00-18:00","Available");
        Treatment eliseTreatment11 = new Treatment("Acupuncture","Consulting Suite B",elise,"Tuesday 25th May 2021, 13:00-15:00","Available");
        Treatment eliseTreatment12 = new Treatment("Neural mobilisation","Consulting Suite B",elise,"Wednesday 26th May 2021, 10:00-12:00","Available");

        Treatment jessicaTreatment1 = new Treatment("Homeopathic Treatment","Consulting Suite A",jessica,"Tuesday 4th May 2021, 12:00-14:00","Available");
        Treatment jessicaTreatment2 = new Treatment("Homeopathic Treatment","Consulting Suite A",jessica,"Tuesday 4th May 2021, 16:00-18:00","Available");
        Treatment jessicaTreatment3 = new Treatment("Homeopathic Treatment","Consulting Suite A",jessica,"Tuesday 11st May 2021, 12:00-14:00","Available");
        Treatment jessicaTreatment4 = new Treatment("Homeopathic Treatment","Consulting Suite A",jessica,"Tuesday 11st May 2021, 16:00-18:00","Available");
        Treatment jessicaTreatment5 = new Treatment("Homeopathic Treatment","Consulting Suite A",jessica,"Tuesday 18th May 2021, 12:00-14:00","Available");
        Treatment jessicaTreatment6 = new Treatment("Homeopathic Treatment","Consulting Suite A",jessica,"Tuesday 18th May 2021, 16:00-18:00","Available");
        Treatment jessicaTreatment7 = new Treatment("Homeopathic Treatment","Consulting Suite A",jessica,"Tuesday 25th May 2021, 12:00-14:00","Available");
        Treatment jessicaTreatment8 = new Treatment("Homeopathic Treatment","Consulting Suite A",jessica,"Tuesday 25th May 2021, 16:00-18:00","Available");

        Treatment jayTreatment1 = new Treatment("Pool rehabilitation","Swimming Pool",jay,"Saturday 1st May 2021, 9:00-11:00","Available");
        Treatment jayTreatment2 = new Treatment("Pool rehabilitation","Swimming Pool",jay,"Saturday 1st May 2021, 13:00-15:00","Available");
        Treatment jayTreatment3 = new Treatment("Massage","Gym",jay,"Wednesday 5th May 2021, 10:00-12:00","Available");
        Treatment jayTreatment4 = new Treatment("Pool rehabilitation","Swimming Pool",jay,"Saturday 8th May 2021, 9:00-11:00","Available");
        Treatment jayTreatment5 = new Treatment("Pool rehabilitation","Swimming Pool",jay,"Saturday 8th May 2021, 13:00-15:00","Available");
        Treatment jayTreatment6 = new Treatment("Massage","Gym",jay,"Wednesday 12th May 2021, 10:00-12:00","Available");
        Treatment jayTreatment7 = new Treatment("Pool rehabilitation","Swimming Pool",jay,"Saturday 15th May 2021, 9:00-11:00","Available");
        Treatment jayTreatment8 = new Treatment("Pool rehabilitation","Swimming Pool",jay,"Saturday 15th May 2021, 13:00-15:00","Available");
        Treatment jayTreatment9 = new Treatment("Massage","Gym",jay,"Wednesday 19th May 2021, 10:00-12:00","Available");
        Treatment jayTreatment10 = new Treatment("Pool rehabilitation","Swimming Pool",jay,"Saturday 22nd May 2021, 9:00-11:00","Available");
        Treatment jayTreatment11 = new Treatment("Pool rehabilitation","Swimming Pool",jay,"Saturday 22nd May 2021, 13:00-15:00","Available");
        Treatment jayTreatment12 = new Treatment("Massage","Gym",jay,"Wednesday 26th May 2021, 10:00-12:00","Available");
        Treatment jayTreatment13 = new Treatment("Pool rehabilitation","Swimming Pool",jay,"Saturday 29th May 2021, 9:00-11:00","Available");


        availableTreatments[0] = robertTreatment1;
        availableTreatments[1] = robertTreatment2;
        availableTreatments[2] = robertTreatment3;
        availableTreatments[3] = robertTreatment4;
        availableTreatments[4] = robertTreatment5;
        availableTreatments[5] = robertTreatment6;
        availableTreatments[6] = robertTreatment7;
        availableTreatments[7] = robertTreatment8;
        availableTreatments[8] = robertTreatment9;
        availableTreatments[9] = robertTreatment10;
        availableTreatments[10] = robertTreatment11;
        availableTreatments[11] = robertTreatment12;
        availableTreatments[12] = robertTreatment13;
        availableTreatments[13] = andhrewTreatment1;
        availableTreatments[14] = andhrewTreatment2;
        availableTreatments[15] = andhrewTreatment3;
        availableTreatments[16] = andhrewTreatment4;
        availableTreatments[17] = andhrewTreatment5;
        availableTreatments[18] = andhrewTreatment6;
        availableTreatments[19] = andhrewTreatment7;
        availableTreatments[20] = andhrewTreatment8;
        availableTreatments[21] = andhrewTreatment9;
        availableTreatments[22] = andhrewTreatment10;
        availableTreatments[23] = andhrewTreatment11;
        availableTreatments[24] = andhrewTreatment12;
        availableTreatments[25] = joeTreatment1;
        availableTreatments[26] = joeTreatment2;
        availableTreatments[27] = joeTreatment3;
        availableTreatments[28] = joeTreatment4;
        availableTreatments[29] = joeTreatment5;
        availableTreatments[30] = joeTreatment6;
        availableTreatments[31] = joeTreatment7;
        availableTreatments[32] = joeTreatment8;
        availableTreatments[33] = joeTreatment9;
        availableTreatments[34] = joeTreatment10;
        availableTreatments[35] = joeTreatment11;
        availableTreatments[36] = joeTreatment12;
        availableTreatments[37] = eliseTreatment1;
        availableTreatments[38] = eliseTreatment2;
        availableTreatments[39] = eliseTreatment3;
        availableTreatments[40] = eliseTreatment4;
        availableTreatments[41] = eliseTreatment5;
        availableTreatments[42] = eliseTreatment6;
        availableTreatments[43] = eliseTreatment7;
        availableTreatments[44] = eliseTreatment8;
        availableTreatments[45] = eliseTreatment9;
        availableTreatments[46] = eliseTreatment10;
        availableTreatments[47] = eliseTreatment11;
        availableTreatments[48] = eliseTreatment12;
        availableTreatments[49] = jessicaTreatment1;
        availableTreatments[50] = jessicaTreatment2;
        availableTreatments[51] = jessicaTreatment3;
        availableTreatments[52] = jessicaTreatment4;
        availableTreatments[53] = jessicaTreatment5;
        availableTreatments[54] = jessicaTreatment6;
        availableTreatments[55] = jessicaTreatment7;
        availableTreatments[56] = jessicaTreatment8;
        availableTreatments[57] = jayTreatment1;
        availableTreatments[58] = jayTreatment2;
        availableTreatments[59] = jayTreatment3;
        availableTreatments[60] = jayTreatment4;
        availableTreatments[61] = jayTreatment5;
        availableTreatments[62] = jayTreatment6;
        availableTreatments[63] = jayTreatment7;
        availableTreatments[64] = jayTreatment8;
        availableTreatments[65] = jayTreatment9;
        availableTreatments[66] = jayTreatment10;
        availableTreatments[67] = jayTreatment11;
        availableTreatments[68] = jayTreatment12;
        availableTreatments[69] = jayTreatment13;

        availableTreatmentsList = new ArrayList<Treatment>(Arrays.asList(availableTreatments));


        boolean bookingLoop = true;
        do{
            System.out.println("Welcome to the Physiotherapy & Sports Injury Centre (PSIC)!");
            System.out.println("Hi, Are you a Visitor or a Patient?");
            System.out.println("1. Visitor");
            System.out.println("2. Patient");
            System.out.println("3. Produce Report 1");
            System.out.println("4. Produce Report 2");
            System.out.println("5. Quit (Produces End of the Term Reports)");
            int input = Integer.valueOf(scanner.nextLine());
            if(input == 1){
                System.out.println("Please enter your name: ");
                String patientName = scanner.nextLine();
                visitorBookingSystem(patientName);
            }else if(input == 2){
                patientBookingSystem();
            }else if(input == 3) {
                System.out.println("REPORT 1 (By Each Appointment): ");
                report1();
            }else if(input == 4){
                System.out.println("REPORT 2 (By Each Patient): ");
                report2();
            }else if(input == 5){
                System.out.println("Thanks for visiting the Physiotherapy & Sports Injury Centre (PSIC)!");
                bookingLoop = false;
            }else{
                System.out.println("Invalid input, try again!");
                continue;
            }
        }
        while(bookingLoop);
        System.out.println("");
        System.out.println("REPORT 1 (By Each Appointment): ");
        report1();
        System.out.println("");
        System.out.println("REPORT 2 (By Each Patient): ");
        report2();
    }
}

